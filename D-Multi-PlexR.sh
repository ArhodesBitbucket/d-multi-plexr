#!/bin/bash

#This script assists with demultiplexing and clipping degenerate primers from metabarcoding projects.
#Various length barcodes and multiple versions of the primer neeed to be searched efficiently.

##############################################################
##DEFINE 1st INPUT = barcode & 2nd INPUT = PEAR.Assembled.fastq
##############################################################

barcodefile=$1
echo "Barcode File is "$barcodefile
export barcodefile

fastqfile=$(basename $2)
echo "Input fastq file is "$fastqfile
export fastqfile


######################################################################
#ERROR MESSAGE IF LESS THAN 2 ARGUMENTS SUPPLIED ON THE COMMAND LINE
######################################################################

if [ $# -le 1 ]

	then

	echo "This script is run with two arguments: a list of barcodesand the fastq file or files to be analyzed."
	echo "Example:  D-Multi-PlexR.sh example.keyfile.txt path/to/FastqDirectory/*.fastq"
	echo ""
	echo "The barcode file has to have columns that contain the following information in this order:"
	echo "SampleID<TAB>LIB_INDEX<TAB>LIBNAME<TAB>FORWARD BARCODE<TAB>REVERSE BARCODE<TAB>FORWARD PRIMER<TAB>REVERSE PRIMER<TAB>LOCUS"
	echo ""	
	echo "The results are output into directories that are named for the LIBNAME."
	exit 1
fi

######################################################################
#MESSAGE IF HELP REQUESTED
######################################################################
#check whether user has supplied -h or --help.  If yes display usage

if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
        echo "This script is run with two arguments: a list of barcodesand the fastq file or files to be analyzed."
        echo "Example:  D-Multi-PlexR.sh example.keyfile.txt path/to/FastqDirectory/*.fastq"
        echo ""
        echo "The barcode file has to have columns that contain the following information in this order:"
        echo "SampleID<TAB>LIB_INDEX<TAB>LIBNAME<TAB>FORWARD BARCODE<TAB>REVERSE BARCODE<TAB>FORWARD PRIMER<TAB>REVERSE PRIMER<TAB>LOCUS"
        echo ""
        echo "The results are output into directories that are named for the LIBNAME."
        exit 0
fi 	
######################################################################
##CREATE BACKUP FOLDER
######################################################################

#Create backup folder for old runs and move old Experimental Results into the folder

	BACKUP=$(echo "Backup."$(date +%Y%m%d))
	export BACKUP
	rm -rf $BACKUP
	[ -d $BACKUP ] || mkdir -p $BACKUP
	
	DIR=$(echo "LIB_*")
	export DIR
	[[ -d $DIR ]] || find $DIR -maxdepth 0 -type d -exec mv -t $BACKUP {} + 2>/dev/null
		
#		find ./LIB_* -maxdepth 1 -type d -exec mv -t $BACKUP {} +


#echo "The program is reading the barcode line before searching the fastq files for matches"

#The first argument that you need to put on the command line is the barcode file.
#The barcode file has to have columns that contain the following information in this order:
#SampleID,LIB_INDEX,LIBNAME,FORWARD BARCODE, REVERSE BARCODE, FORWARD PRIMER, REVERSE PRIMER, LOCUS
#This may work better if the barcode file does not have a header.

#To separate replicates, please do not use underscore in the sample names unless it is before the letter indicating a replicate.
#For example, JA_9_a should be changed to JA-9_a.  This will facilitate the separation of sample replicates into the same folder, named JA-9. 

#The second argument designates which fastq file to work on.  The program is written for fastq at the moment, retaining fastq outputs.  This can be changed later.
#To designate multiple fastq files, wildcards are okay: path/to/mydirectory/*.fastq

#Remove previous results from the directory.  This could be commented out, but be aware that
#files are set to append data, not overwrite data, so old files need to be removed before running or
#take the comment off this block.

#for i in LIB*/;do rm -rf $i;done

#To run the Demultiplexer, a list of barcodes needs to be submitted.  This is "Barcodes_C.txt"
#Future projects need to enter the data in the same manner to the barcode file.
#SAMPLEID<TAB>LIBINDEX<>EXPERIMENTNAME<TAB>FORWARDBARCODE<TAB>REVERSEBARCODE<TAB>FORWARDPRIMER<TAB>REVERSEPRIMER<TAB>LOCUS
#EXAMPLE --- NOTE: NO HEADERS
#PM10_a  ACAGTG C        TGCAAGACGT      ACAACAGCATGA    TTAGATACCCCACTATGC      YAGAACAGGCTCCTCTAG      12s
#PM10_b  ACAGTG C        ACTCTGAGCGA     ACAACAGCATGA    TTAGATACCCCACTATGC      YAGAACAGGCTCCTCTAG      12s
#PM10_c  ACAGTG C        TATGCGGTATGA    ACAACAGCATGA    TTAGATACCCCACTATGC      YAGAACAGGCTCCTCTAG      12s

#A subsample of the marten_new_ACAGTG.pear.fastq.assembled.fastq created October 12th was created using the code in 1026createsubsample.txt.
#The original pearrun command and program version can be found in "Martin_new_pear_command.txt in this repository.
#This code is run by typing "DEMULTIPLEXER.v3.sh Barcodes_C.txt"

#This reads in each line of the Barcodes_C.txt and acts on the combination of barcodes and primers.
#Future code will use the "set" in python to make the combination of primers/barcodes into a searchable set, then run the fastq through as a multithreaded program.

#Okay, fun fact.  Some of the forward/reverse barcodes are reverse complements of each other.
#That makes it difficult to determine which sequences are being read in reverse based only on barcode.
#The sequences that have a forward barcode/reverse primer at the beginning of the line are the sequences that need to be reversed before clustering.

###########################################################
##SEQUENCE CLEANER FOR FASTQ AFTER PEAR
###########################################################
	#For each fastq file, we want to calculate the median and reject sequences that are 20 bp below and 20 bp above the mode.
	#these sequences were likely misassembled by pear, and can cause problems downstream in the clustering
	#I originally tried median, but if there is a bimodal distribution, this does not work, so
	#I switched to mode to find the most frequently observed length, which guides the minimum and maximum lengths of sequences to accept.  This also does not need to be rounded.

	mode=$(echo $(head -n 100000 $2 | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' | sort -nk2 | tail -n1 | awk '{print $1}'))
	export mode
	lowerlimit=$(($mode -20))
	export lowerlimit
	upperlimit=$(($mode+20))
	export upperlimit


#This next line creates a temporary folder where the large fastq files are split up and used in the parallel processing step.
	temp=$(echo "temp"$(date +%Y%m%d))
	export temp
#	rm -rf ./temp*/*
#	rm -rf ./temp*
	[ -d $temp ] || mkdir $temp

#to recognize where the temp folder is for splitting, we define the current directory which contains the temp directory
#the temp directory is not automatically removed at the end of the run, but it is recommended to remove it if the splitting/reversing needs to be redone. 
#Otherwise, turn off the line below that resplits and rereverses everything, this only needs to be done once.


#####################################
##SPLITTING FASTQ FILES IN PARALLEL
#####################################
#January 2, 2017 note - the splitting process is slowed down by the size selection and conversion of the multiline fastq into a single line fastq.
#I sped it up by parallelization.
#This function uses the calculated mode to size select sequences from the original fastq.
#This function also converts multiline fastq into single line fastq for faster grepping.



sizeselect () {
		fastqfile=$1
		export fastqfile
		awk '/^@/ {printf("\n%s\t",$0);next; } { printf("%s\t",$0);}  END {printf("\n");}' $fastqfile |\
		sed 's/ 1:N:0/:1:N:0/g' | sed '/^$/d' |\
		awk -v ll="$lowerlimit" -v ul="$upperlimit" 'length($2)>ll && length($2)<ul' 
}

export -f sizeselect

#This function splits up the single line fastq that has been screened for sequences within 20 bp length of the mode.
#The amount of lines for splitting (-l argument) can become an argument for the wrapper script.




#The first parallel calls the sizeselect function which removes "pear" sequences outside 20 bp within the mode as well as makes the fastq single line.
#The second parallel splits up the size selected single line fastq into multiple files based on the number of lines specified in the function.

echo "Reading in and splitting pear-assembled fastq file $fastqfileargument - files will be saved in temp directory for now"

free=$(awk '/^((Swap)?Cached|MemFree|Buffers):/ { sum += $2 }END{ print sum }' /proc/meminfo)
percpu=$((free / 10 / $(parallel --number-of-cores)))k



#<$2 /local/cluster/bin/parallel --will-cite --pipe -N 4000000 --blocksize $percpu --compress sizeselect | parallel --will-cite --pipe -N 1000000 --blocksize $percpu cat ">" $temp/$fastqfile.split.{#}

#<$2 /local/cluster/bin/parallel --will-cite --pipe -j20 -N 4000000 --blocksize=$blockpercpu --max-lines=6000000 --compress sizeselect | parallel --will-cite --pipe --blocksize=$blockpercpu -j20 -N 10000 cat ">" $temp/$fastqfile.{#}.split


###############################################################################
##THIS FOLLOWING LINE IS THE LINE TO COMMENT OUT TO PREVENT ANOTHER SPLIT
###############################################################################
<$2 /local/cluster/bin/parallel --will-cite --pipe -j10 -L 400000 --blocksize=$percpu --compress sizeselect | parallel --will-cite --pipe --blocksize=$percpu -j2 -L 10000 cat ">" $temp/$fastqfile.{#}.split

###############################################################################

#Now we have several single-line fastq files inside the temp directory in the scratch space.  


##############################################################
##IDENTIFYING SEQUENCES WITH REVERSE PRIMER SET AND REVERSING
##############################################################
#The next few lines make a temporary file that specifies the anchors (line position) of the primers based on the length of the barcodes (between 8 and 12 in this case).

rm fwsearch.txt 2>/dev/null
rm fwsearch.temp.txt 2>/dev/null
rm revsearch.txt 2>/dev/null
rm revsearch.temp.txt 2>/dev/null

###########################################
#2016 VERSION OF THE REVERSING FUNCTION
###########################################

# awk '{print $7}' SetA_Barcode.txt | sed 's/Y/[CT]/g' | sed 's/W/[AT]/g' | rev | tr 'ACGT[]' 'TGCA][' | sed 's/]/]{1}/g'> fwsearch.temp.txt
# 
# #Version for the old barcodes without primers and with variable length barcodes
# awk '{print $6}' $barcodefile | sed 's/Y/[CT]{1}/g' | sed 's/W/[AT]{1}/g' | paste - fwsearch.temp.txt | sed 's/\t/.*/g' | sed -e 's/^/[[:space:]][CTGAN]{8,12}/g' | sed -e 's/$/[CTGAN]{8,12}[[:space:]]/g' | sort | uniq > fwsearch.txt
# 
# awk '{print $6}' $barcodefile  |sed 's/Y/[CT]/g'|sed 's/W/[AT]/g'| rev|tr 'ACGT[]' 'TGCA][' | sed 's/]/]{1}/g' > revsearch.temp.txt
# 
# #Version for the old barcodes without primers and with variable length barcodes
# awk '{print $7}' $barcodefile | sed 's/Y/[CT]{1}/g' | sed 's/W/[AT]{1}/g' | paste - revsearch.temp.txt | sed 's/\t/.*/g' | sed -e 's/^/[[:space:]][CTGAN]{8,12}/g' | sed -e 's/$/[CTGAN]{8,12}[[:space:]]/g' | sort | uniq > revsearch.txt
# 
###########################################
#2017 VERSION OF THE REVERSING FUNCTION
###########################################

awk '{print $7}' $barcodefile | sed 's/Y/[CT]/g' | sed 's/W/[AT]/g' | rev | tr 'ACGT[]' 'TGCA][' | sed 's/]/]{1}/g'> fwsearch.temp.txt

#Version for the new spacers on 3/22/2017, 0-3 spacers + 8 barcode = 8-11
awk '{print $6}' $barcodefile | sed 's/Y/[CT]{1}/g' | sed 's/W/[AT]{1}/g' | paste - fwsearch.temp.txt | sed 's/\t/.*/g' | sed -e 's/^/[[:space:]][CTGAN]{8,11}/g' | sed -e 's/$/[CTGAN]{8,11}[[:space:]]/g' | sort | uniq > fwsearch.txt

awk '{print $6}' $barcodefile  |sed 's/Y/[CT]/g'|sed 's/W/[AT]/g'| rev|tr 'ACGT[]' 'TGCA][' | sed 's/]/]{1}/g' > revsearch.temp.txt

#Version for the new spacers on 3/22/2017
awk '{print $7}' $barcodefile | sed 's/Y/[CT]{1}/g' | sed 's/W/[AT]{1}/g' | paste - revsearch.temp.txt | sed 's/\t/.*/g' | sed -e 's/^/[[:space:]][CTGAN]{8,11}/g' | sed -e 's/$/[CTGAN]{8,11}[[:space:]]/g' | sort | uniq > revsearch.txt


#The next two functions used the temporary reverse and forward primer files created above to search through the split files and reverse any sequences containing a reverse complement of the primer pairs.
#{1} means exactly one of the characters inside the brackets [AT], so either A or T, but not AT will meet the requirement.  Without {1}, AT would match as well.
#{8,12} means any number of characters between and including 8 and 12.

#This function finds primers in the forward direction and in the right position in the line that are not reversed, and passes the line to the output file.

reverserfw () {
		sizesplitinput=$1
		#echo "reverserfw $sizesplitinput"
		cat fwsearch.txt | while read line;do
			LC_ALL=C agrep -E5 -e $line $sizesplitinput >> $sizesplitinput.rev ;
		done
}


export -f reverserfw

reverserrev() {
		sizesplitinput=$1
		#echo "reverserrev $sizesplitinput"
		cat revsearch.txt | while read line; do
			LC_ALL=C agrep -E5 -e $line $sizesplitinput |\
				awk 'BEGIN{FS="\t"}NR>0{gsub (/A/,"a",$2)}{gsub(/T/,"t",$2)}{gsub(/G/,"g",$2)}{gsub(/C/,"c",$2)}{gsub(/c/,"G",$2)}{gsub(/g/,"C",$2)}{gsub(/a/,"T",$2)}{gsub(/t/,"A",$2)}{for(i=length($2);i!=0;i--)x=x substr($2,i,1)}{for(j=length($4);j!=0;j--)y=y substr($4,j,1)}{print $1 "\t" x "\t" $3 "\t" y ;x="";y=""}' >> $sizesplitinput.rev ;
		done
}

export -f reverserrev


##NOTE: Future versions of the command could use the locus ID to separate out reversed files into one for each locus, which then could limit the need for using all barcodes against the search set.  Only the corresponding locus barcodes would be needed to match.

#Another way to parallel commands using 'xargs' -- this is actually faster than parallel
#for this particular set of functions.  This takes the two functions we just defined and uses it to make all primers be in the forward direction.


echo "Identifying lines that are reversed and rewriting them in the forward direction"

find . -name *fastq.*.split | xargs -n 1 -P 20 bash -c 'reverserrev "$@" >> "$@".rev' _ 
find . -name *fastq.*.split | xargs -n 1 -P 20 bash -c 'reverserfw "$@" >> "$@".rev' _ 



#This is the old version of the parallel to find reversed lines, it was slower, so removed	
#parallel_reverser () {

#		j=$1
###Next three lines were commented out, but provide another alternative
##		echo "Searching $j for reverse lines"
##		/local/cluster/bin/parallel -j10 --will-cite -N10000 reverserfw ::: $j >> $j.rev
##		/local/cluster/bin/parallel -j10 --will-cite -N10000 reverserrev ::: $j >> $j.rev

#		reverserfw $j >> $j.rev
#		reverserrev $j >> $j.rev
#}
#export -f parallel_reverser

#parallel -j10 --will-cite --progress parallel_reverser ::: ./$temp/*.split.*


###########################################
#2016 VERSION OF THE ASSIGNMENT FUNCTION
###########################################

# demforward () {
# 
# 	filename=$1
#
# searchstring=$(echo "[[:space:]]$n_f_barcode.*$n_rc_r_barcode[[:space:]]")	
# export searchstring 
# #TURN ON IF TWO PRIMER SETS
# #LC_ALL=C agrep -E5 -e $f_primer_adj.*.$rc_r_primer_adj $filename |\
# 	LC_ALL=C egrep -e "$searchstring" $filename |\
# 		while read sequence;do echo $sequence| \
# 		awk -v varf="$forward_sub" -v varr="$reverse_sub" -v reml="$removed_length" \
# 		'{print $1 "\t" substr($2,varf,length($2)-reml) "\t" $3 "\t" substr($4,varf,length($4)-reml)}' |\
# 		 awk 'BEGIN{FS="\t"}NR>0{print $1 "\n" $2 "\n" $3 "\n" $4}' ;done
# 
# }
# 
# export -f demforward

###########################################
#2017 VERSION OF THE ASSIGNMENT FUNCTION
###########################################
#This only works on equal length barcodes, rever to the old version if variable length barcodes are used.

demforward () {

	filename=$1
	searchstring=$(echo "[[:space:]][CGATN]{0,3}$n_f_barcode$f_primer_4.*$n_rc_r_barcode[CGATN]{0,3}[[:space:]]")	
	export searchstring 
	echo $searchstring

#The addition of the $f_primer_4 after the barcode adds the first 4 base pairs of the primer to the end of the barcode, and prevents duplicate assignments.

#######The next line, which is commented out, needs to be turned back on if more than one primer is in the fwsearch.txt or revsearch.txt files.
#	LC_ALL=C agrep -E5 -e $f_primer_adj".*"$rc_r_primer_adj $filename|\

	LC_ALL=C egrep -e "$searchstring" $filename |\
	while read sequence;do  
	startposition=$(echo $sequence |  awk '{print $2}'| grep  -aob $n_f_barcode | cut -d ":" -f1)
	export startposition
	##echo $startposition
	endposition=$(echo $sequence |  awk '{print $2}' | grep -aob $n_rc_r_barcode | cut -d ":" -f1)
	export endposition
	##echo $endposition
	##echo $sequence


	
##	echo $sequence | awk -v varf="$startposition" -v forsub="$forward_sub" -v 	varr="$endposition"  -v revsub="$reverse_sub" \ '{begin=varf+forsub}{splen=length($2)-varr}{end=length($2)-splen-revsub-begin}{print $1 "\t" substr($2,begin,end) "\t" $3 "\t" substr($4,begin,end)}' | awk 'BEGIN{FS="\t"}NR>0{print $1 "\n" $2 "\n" $3 "\n" $4}' 


echo $sequence | awk -v varf="$startposition" -v forsub="$forward_sub" -v 	varr="$endposition"  -v lenrp="$length_rp" '{begin=varf+forsub+1}{splen=length($1)-varr}{end=length($1)-splen-lenrp-begin+1}{print $1"\t"substr($2,begin,end)"\t"$3"\t"substr($4,begin,end)}' | awk 'BEGIN{FS="\t"}NR>0{print $1 "\n" $2 "\n" $3 "\n" $4}'


##{print varf "\t"forsub"\t"lenrp"\t"splen"\t"begin"\t"end}'


done
}

export -f demforward

#Because there are spacers in the latest set of primers, the spacers need to be removed by selecting exactly the text including and between the barcodes.
#This line finds the starting position of the barcode in the line
#egrep -e "[[:space:]][NCGAT]{0,3}GCATCCTA.*TAGGATGC[NCAGT]{0,3}[[:space:]]" | grep -aob GCATCCTA | cut -d ":" -f1
#This line finds the starting position of the reverse barcode
#egrep -e "[[:space:]][NCGAT]{0,3}GCATCCTA.*TAGGATGC[NCAGT]{0,3}[[:space:]]" | grep -aob TAGGATGC | cut -d ":" -f1
#To find the place to cut the sequence, add the length of the forward barcode and forward primer to the starting position of the forward barcode; then, subtract the length of the reverse primer from the starting position of the reverse barcode.


#########################################################################
#THIS FUNCTION IS NOT NEEDED BECAUSE ALL SEQUENCES ARE REVERSED BEFORE ASSIGNMENT NOW.
#THIS CODE IS JUST HERE FOR FUTURE REFERENCE IN CASE IT IS NEEDED FOR ANOTHER REASON.

# demreverse () {
# 		
# 	filename=$1
# #	echo $filename
# 	LC_ALL=C agrep -E5 -e $r_primer_adj".*"$rc_f_primer_adj $filename|\
# #This next line is the alternate grep in case of only screening for an "N" substitution at the beginning of the barcodes"
# #	LC_ALL=C egrep "$r_barcodefn|$n_r_barcode" | LC_ALL=C egrep "$rc_f_barcode|$n_rc_f_barcode"|\
# 	LC_ALL=C egrep "$n_r_barcode" | LC_ALL=C egrep "$n_rc_f_barcode"|\
# 	while read sequence;do 
# #This line trims the barcodes and primers and places the new sequence in the appropriate fastq file.
# 	echo $sequence | awk -v varf="$forward_sub" -v varr="$reverse_sub" -v reml="$removed_length" '{print $1"\t"substr($2,varf,length($2)-reml)"\t"$3"\t" substr($4,varf,length($4)-reml)}' |\
# 	awk 'BEGIN{FS="\t"}{RS="\n"}NR>0{gsub (/A/,"a",$2)}{gsub(/T/,"t",$2)}{gsub(/G/,"g",$2)}{gsub(/C/,"c",$2)}{gsub(/c/,"G",$2)}{gsub(/g/,"C",$2)}{gsub(/a/,"T",$2)}{gsub(/t/,"A",$2)}{for(i=length($2);i!=0;i--)x=x substr($2,i,1)}{for(j=length($4);j!=0;j--)y=y substr($4,j,1)}{print $1 "\n" x "\n" $3 "\n" y ;x="";y=""}' ;done
# }
# 
# export -f demreverse
#########################################################################


##############################################################
##LOOPING THROUGH THE BARCODES - EACH JOB = ONE BARCODE
##############################################################

#The next function reads in the barcodes and parses the into their component parts.
#The original while loop has been replaced with a parallel command that calls a function called "setvalues" that acts on the barcode file [argument $1 on the command line]

setvalues () {

#This reads the first line of the barcode file and sets the values for searching in the fastq files
#cat $1|\
echo "$1" |\
while read sample lib_index lib f_barcode r_barcode f_primer r_primer locus

do

#These lines create shell variables that can be used within the bash functions demforward and demreverse, created below.

 	libfn=$lib
 	export libfn
# 	echo $libfn
 	f_barcodefn=$f_barcode
 	export f_barcodefn
 #	echo $f_barcodefn
  	r_barcodefn=$r_barcode
	export r_barcodefn
	f_primerfn=$f_primer
	export f_primerfn
	r_primerfn=$r_primer
	export r_primerfn
	locusfn=$locus
	export locusfn


#This first line creates a sample ID that will be used as the new file name.  Each file will be named $SampleID.fastq.  Each barcode combination will create a fastq.

        SampleID=$(echo $sample"_LIB_"$lib"_FBAR_"$f_barcode"_RBAR_"$r_barcode"_LOCUS_"$locus)

export SampleID

#Create subdirectories for results

       	DIR1=$(echo "LIB_"$lib)
      	export DIR1
       	[ -d $DIR1 ] || mkdir -p $DIR1

	
	DIR2=$(echo "LIB_"$lib/$locus)
	export DIR2
	[ -d $DIR2 ] || mkdir -p $DIR2

#This next step can create directories based on main sample names, but only if
#the convention of naming samples uses an underscore to separate reps
#For example AG1_a, AG1_b --- this will create a directory called "AG1" inside the locus directory, which contains results for AG1_a and AG1_b.

	norep=$(echo $sample | grep -o '^[^_]*')
	export norep
		
	DIR3=$(echo "LIB_"$lib/$locus/$norep)
	export DIR3
	echo $DIR3
	[ -d $DIR3 ] || mkdir -p $DIR3

#Sample output for echo $SampleID
#PM10_a_LIB_C_FBAR_TGCAAGACGT_RBAR_ACAACAGCATGA_LOCUS_12s
#PM10_b_LIB_C_FBAR_ACTCTGAGCGA_RBAR_ACAACAGCATGA_LOCUS_12s
#PM10_c_LIB_C_FBAR_TATGCGGTATGA_RBAR_ACAACAGCATGA_LOCUS_12s

#In addition, a directory based on the experiment name will be created.  All fastq files for that experiment will be placed in that directory.
#In this case: LIB_C 

#In order to make sure that sequences are placed in the correct bin, we have to find the reverse sets of all primers to combine with the forward sets.

#Note to self:  I wonder if the combination of primer/barcode could be used instead of doing this separately?  But I guess there is no way to know if a forward barcode
#landed on a reverse primer in library prep, is there?

#Make reverse complements of our primer sets

	rc_f_primer=$(echo $f_primer|rev|tr ACGT TGCA)
	rc_r_primer=$(echo $r_primer|rev|tr ACGT TGCA)

	export rc_f_primer
	export rc_r_primer

#Make reverse complements of our barcodes

        rc_f_barcode=$(echo $f_barcode|rev|tr ACGT TGCA)
        rc_r_barcode=$(echo $r_barcode|rev|tr ACGT TGCA)

	export rc_f_barcode
	export rc_r_barcode

#I ran a test to make sure it worked correctly using the following code:

#echo "Forward Primer: " $sample $f_primer "Reverse Complement Forward Primer: " $rc_f_primer| head
#echo "Reverse Primer: " $sample $r_primer "Reverse Complement Reverse Primer: " $rc_r_primer
#echo "Forward Barcode: " $sample $f_barcode "Reverse Complement Forward Barcode: " $rc_f_barcode
#echo "Reverse Barcode: " $sample $r_barcode "Reverse Complement Reverse Barcode: " $rc_r_barcode

#Here is the output:

#Forward Primer:  PM10_a TTAGATACCCCACTATGC Reverse Complement Forward Primer:  GCATAGTGGGGTATCTAA
#Reverse Primer:  PM10_a YAGAACAGGCTCCTCTAG Reverse Complement Reverse Primer:  CTAGAGGAGCCTGTTCTY
#Forward Barcode:  PM10_a TGCAAGACGT Reverse Complement Forward Barcode:  ACGTCTTGCA
#Reverse Barcode:  PM10_a ACAACAGCATGA Reverse Complement Reverse Barcode:  TCATGCTGTTGT

#Some barcodes and the library index had N or an incorrect nucleotide for the first base, so we need to make a search pattern that account for those.
#What the regular expression does is search for the original barcode, with a regular expression at the beginning of the line.
#[NCGAT] means that for this one nucleotide, any of the letters in the brackets will be accepted as a regular expression.
#This also happened with the $lib_index in some cases, so we added the possibilites of incorrect nucleotides in the first position to that variable as well.


#This is the code that was retired 1/4 which allowed for any base substitution in the first position.
#This version of the code caused duplicate assignments
#        n_f_barcode=$(echo $f_barcode | sed 's/^./[NCGAT]/g')
#        n_r_barcode=$(echo $r_barcode | sed 's/^./[NCGAT]/g')
#        n_rc_f_barcode=$(echo $rc_f_barcode | sed 's/^./[NCGAT]/g')
#        n_rc_r_barcode=$(echo $rc_r_barcode | sed 's/^./[NCGAT]/g')
#        n_lib_index=$(echo $lib_index | sed 's/^./[NCGAT]/g')


##################################################################
##2016 VERSION OF HOW TO DEFINE FORWARD BARCODE WITH AMBIGUITY
##################################################################


#Based on feedback on 11/18, I have made an alternate barcode search only include "N" in the first base
#This occurred about 10% of the time.
#About 2% of the barcodes have a different nucleotide than "N" in the first position, but I have now altered the code to not worry about that.
#This will leave these fastq lines unassigned.
#If this version of the code is used, then the alternate grep statement, which is currently commented out, needs to be used. Searches will have to be done using the "or" option - search for $f_barcode or $n_f_barcode
#For example: grep -E 'AGAAGTG|NGAAGTG'



#        n_f_barcode=$(echo $f_barcodefn | sed 's/^./N/g')
#        n_r_barcode=$(echo $r_barcodefn | sed 's/^./N/g')
#        n_rc_f_barcode=$(echo $rc_f_barcode | sed 's/^./N/g')
#        n_rc_r_barcode=$(echo $rc_r_barcode | sed 's/^./N/g')
#        n_lib_index=$(echo $lib_index | sed 's/^./N/g')

#This code was upgraded in March, 2017 to make sure that the ambiguity was exactly at the ends of the sequence.

#        n_f_barcode=$(echo $f_barcodefn | sed 's/^./[N&]/1'| sed 's/\]/\]\{1\}/g')
#        n_r_barcode=$(echo $r_barcodefn | sed 's/^./[N&]/1'| sed 's/\]/\]\{1\}/g')
#        n_rc_f_barcode=$(echo $rc_f_barcode| sed 's/.$/[N&]/1'| sed 's/\]/\]\{1\}/g')
#        n_rc_r_barcode=$(echo $rc_r_barcode| sed 's/.$/[N&]/1'| sed 's/\]/\]\{1\}/g')
#        n_lib_index=$(echo $lib_index | sed 's/^./[N&]/1 ' | sed 's/\]/\]\{1\}/g')
# 


##################################################################
##2017 VERSION OF HOW TO DEFINE FORWARD BARCODE WITH AMBIGUITY
##################################################################
#This next five lines don't work anymore, because there is an overlap between the 0-3 mismatches
#at the beginning of the line and this ambiguity.  I am not seeing the single bp ambiguity at the ends of barcodes, so I am going to try the code without it for now.

#       n_f_barcode=$(echo $f_barcodefn | sed 's/^./[N&]/1'| sed 's/\]/\]\{1\}/g')
#       n_r_barcode=$(echo $r_barcodefn | sed 's/^./[N&]/1'| sed 's/\]/\]\{1\}/g')
#       n_rc_f_barcode=$(echo $rc_f_barcode| sed 's/.$/[N&]/1'| sed 's/\]/\]\{1\}/g')
#       n_rc_r_barcode=$(echo $rc_r_barcode| sed 's/.$/[N&]/1'| sed 's/\]/\]\{1\}/g')
#       n_lib_index=$(echo $lib_index | sed 's/^./[N&]/1 ' | sed 's/\]/\]\{1\}/g')


#This next five lines also don't solve the problem, because some of the barcodes overlap and the small mismatch before the barcodes means that they result in duplicate assignments.
#For example:  CAGGTTCA can be confused with GTCCTAAG because we allow three mismatches before the start of the barcode, so GT in front of CCTAAGTC --> GTCCTAAG

#For example:
#25,000 fastq sequences
#24,441 pass size selection
#21,743 have forward primers that start on or between the 8th and 12th positions.
#13,312 assigned to barcodes
#10 out of 13,322 assigned to two barcodes - when the first four basepairs of the primer are not included to prevent duplication.
#13,294

#Two options - only allow exactly three mismatches before the barcode
#Or, put a few bp from the primer behind the barcode to make sure it is directly adjacent
#Checking option 1 (in bash): - if not in bash, type bash at command line
#egrep -e "^[CTGAN]{8,12}TTAGATACCCCACTATGC.*CTAGAGGAGCCTGTTCT[AG]{1}[CTGAN]{8,12}$" ../../../PAIRED/SetA.pear.fastq.assembled.fastq  | egrep -e "^[CGATN]{0,3}CAGGTTCA.*TGAACCTG[CGATN]{0,3}$" | head -n10000 | while read line ; do echo $line | grep -aob CAGGTTCA | cut -d ":" -f1 ; done > numbers.txt
#sort numbers.txt| uniq -c | awk '$2<4 {print $0}'
#     43 0 - spacers
#     69 1 - spacer
#     72 2 - spacers
#   9816 3 - spacers
# By random chance, some of the positions were more than 3 because the sequence can be found inside the barcoded region.  We don't care about those, only the ones that mess up assignment, the current stringency is spacers between 0 and 3 at the beginning of the line.
# These are just the raw counts, if these were all true barcodes, the primer would be behind them, so checking that, using the first four letters of the primer to filter the original set grepped by the barcode to those that probably have a primer behind them(in bash):  - if not in bash, type bash at command line
#egrep -e "^[CTGAN]{8,12}TTAGATACCCCACTATGC.*CTAGAGGAGCCTGTTCT[AG]{1}[CTGAN]{8,12}$" ../../../PAIRED/SetA.pear.fastq.assembled.fastq  | egrep -e "^[CGATN]{0,3}CAGGTTCA.*TGAACCTG[CGATN]{0,3}$"  | head -n10000 | while read line ; do echo $line | grep -aob CAGGTTCATTAG | cut -d ":" -f1 ; done > numbers2.txt
#sort numbers2.txt| uniq -c | awk '$2<4 {print $0}'
#     43 0 - spacers
#     69 1 - spacer
#     72 2 - spacers
#   9812 3 - spacers [6 less than before]

#This code checks where the primers start:
#egrep -e "^[CTGAN]{8,12}TTAGATACCCCACTATGC.*CTAGAGGAGCCTGTTCT[AG]{1}[CTGAN]{8,12}$" ../../../PAIRED/SetA.pear.fastq.assembled.fastq  | egrep -e "^[CGATN]{0,3}CAGGTTCA.*TGAACCTG[CGATN]{0,3}$" | head -n10000 | while read line ; do echo $line | grep -aob TTAGATACCCCACTATGC | cut -d ":" -f1 ; done > numbers3.txt
#sort numbers3.txt| uniq -c | awk '$2<13 && $2>7 {print $0}' | sort -nk2
#     43 8  - spacers plus barcode
#     69 9  - spacers plus barcode
#     72 10 - spacers plus barcode
#   9812 11 - spacers plus barcode
#      4 12 - spacers plus barcode plus extra basepair

#Example of what this looks like [spaces put between beginning spacer, barcode, extra basepair, forward primer, sequence, reverse primer, reverse barcode, end spacer

#TGG CAGGTTCA T TTAGATACCCCACTATGC CTGGCCCTAAATCTTGATACTAATATACTCACGTATCCGCCTGAGAACTACGAGCACAAACGCTTAAAACTCTAAGGACTTGGCGGTGCCCTAAACCCAC CTAGAGGAGCCTGTTCTA TGAACCTG ATG
#TGG CAGGTTCA T TTAGATACCCCACTATGC CTGGCCCTAAATCTTGATACTAATATACTCACGTATCCGCCTGAGAACTACGAGCACAAACGCTTAAAACTCTAAGGACTTGGCGGTGCCCTAAACCCAC CTAGAGGAGCCTGTTCTA TGAACCTG ATG
#GTT CAGGTTCA T TTAGATACCCCACTATGC CTGGCCCTAAATCTTGATACTAATATACTCACGTATCCGCCTGAGAACTACGAGCACAAACGCTTAAAACTCTAAGGACTTGGCGGTGCCCTAAACCCAC CTAGAGGAGCCTGTTCTA TGAACCTG TGA
#AGG CAGGTTCA T TTAGATACCCCACTATGC CTGGCCCTAAATCTTGATACTAATATACTCACGTATCCGCCTGAGAACTACGAGCACAAACGCTTAAAACTCTAAGGACTTGGCGGTGCCCTAAACCCAC CTAGAGGAGCCTGTTCTA TGAACCTG GCC

#Keeping these four lines would complicate the trimming, it would all be based on primer start position, which would require using agrep again to recognize the start of the primer sequence.


       n_f_barcode=$(echo $f_barcodefn)
       n_r_barcode=$(echo $r_barcodefn)
       n_rc_f_barcode=$(echo $rc_f_barcode)
       n_rc_r_barcode=$(echo $rc_r_barcode)
       n_lib_index=$(echo $lib_index)


	export n_f_barcode
	export n_r_barcode
	export n_rc_f_barcode
	export n_rc_r_barcode
	export n_lib_index
	

#############################
##2016 CHECK
#############################
#I used the following code to check this:

#echo "Forward Barcode: "$sample $f_barcode "Regex for any version with different first nucleotide: " $n_f_barcode
#echo "Reverse Barcode: "$sample $r_barcode "Regex for any version with different first nucleotide: " $n_r_barcode
#echo "RC Forward Barcode: "$sample $rc_f_barcode "Regex for any version with different first nucleotide: " $n_rc_f_bar$
#echo "RC Reverse Barcode: "$sample $rc_r_barcode "Regex for any version with different first nucleotide: " $n_rc_r_bar$
#echo "Lib_Index: "$sample $lib_index "Regex for any version with different first nucleotide: " $n_lib_index

#Here is the ouput:

#Forward Barcode: PM10_a TGCAAGACGT Regex for any version with different first nucleotide:  [NCGAT]GCAAGACGT
#Reverse Barcode: PM10_a ACAACAGCATGA Regex for any version with different first nucleotide:  [NCGAT]CAACAGCATGA
#RC Forward Barcode: PM10_a ACGTCTTGCA Regex for any version with different first nucleotide:  [NCGAT]CGTCTTGCA
#RC Reverse Barcode: PM10_a TCATGCTGTTGT Regex for any version with different first nucleotide:  [NCGAT]CATGCTGTTGT
#Lib_Index: PM10_a ACAGTG Regex for any version with different first nucleotide:  [NCGAT]CAGTG


##################################################################
##DEFINE DEGENERATE PRIMERS
##################################################################


#Some primers have ambiguous codons in them, so we need to adjust for those before grepping
#This sed statement can be expanded to include other degenerate sets.
#In this case, Y means C or T, W means A or T.

        f_primer_adj=$(echo $f_primer|sed 's/Y/[CT]/g'|sed 's/W/[AT]/g')
        r_primer_adj=$(echo $r_primer|sed 's/Y/[CT]/g'|sed 's/W/[AT]/g')
        rc_f_primer_adj=$(echo $f_primer_adj|rev|tr ACGT[] TGCA][)
        rc_r_primer_adj=$(echo $r_primer_adj|rev|tr ACGT[] TGCA][)

	export f_primer_adj
	export r_primer_adj
	export rc_f_primer_adj
	export rc_r_primer_adj

	f_primer_4=$(echo $f_primer|cut -c1-4 |sed 's/Y/[CT]/g'|sed 's/W/[AT]/g')
	echo $f_primer_4
	export f_primer_4
	#NOTE - this only works if degenerate primer is not in the first four locations. It also ignores when there is one base pair between the barcode and primer
	

#I used the following code to check this:

#echo "Forward Primer: "$sample $f_primer "Forward primer with ambiguous codon replaced " $f_primer_adj
#echo "Reverse Primer: "$sample $r_primer "Reverse primer with ambiguous codon replaced " $r_primer_adj
#echo "RC Forward Primer: "$sample $rc_f_primer "RC forward primer with ambiguous codon replaced " $rc_f_primer_adj
#echo "RC Reverse Primer: "$sample $rc_r_primer "RC reverse primer with ambiguous codon replaced " $rc_r_primer_adj

#Here is the output:

#Forward Primer: PM10_a TTAGATACCCCACTATGC Forward primer with ambiguous codon replaced  TTAGATACCCCACTATGC
#Reverse Primer: PM10_a YAGAACAGGCTCCTCTAG Reverse primer with ambiguous codon replaced  [CT]AGAACAGGCTCCTCTAG
#RC Forward Primer: PM10_a GCATAGTGGGGTATCTAA RC forward primer with ambiguous codon replaced  GCATAGTGGGGTATCTAA
#RC Reverse Primer: PM10_a CTAGAGGAGCCTGTTCTY RC reverse primer with ambiguous codon replaced  CTAGAGGAGCCTGTTCT[AG]

#After using the fuzzy search for agrep, we will need to cut the sequences by length,
#because many different versions of the primer could match our fuzzy search

	length_fb=${#f_barcode}
    length_rb=${#r_barcode}
    length_fp=${#f_primer}
    length_rp=${#r_primer}

	export length_fb
	export length_rb
	export length_fp
	export length_rp

#I used the following code to check this:

#echo "Forward Barcode: " $sample $f_barcode "Length of barcode: "$length_fb
#echo "Reverse Barcode: " $sample $r_barcode "Length of barcode: "$length_rb
#echo "Forward Primer: " $sample $f_primer "Length of primer: "$length_fp
#echo "Reverse Primer: " $sample $r_primer "Length of primer: "$length_rp

#Here is the output:

#Forward Barcode:  PM10_a TGCAAGACGT Length of barcode: 10
#Reverse Barcode:  PM10_a ACAACAGCATGA Length of barcode: 12
#Forward Primer:  PM10_a TTAGATACCCCACTATGC Length of primer: 18
#Reverse Primer:  PM10_a YAGAACAGGCTCCTCTAG Length of primer: 18
#Forward Barcode:  PM10_b ACTCTGAGCGA Length of barcode: 11
#Reverse Barcode:  PM10_b ACAACAGCATGA Length of barcode: 12
#Forward Primer:  PM10_b TTAGATACCCCACTATGC Length of primer: 18
#Reverse Primer:  PM10_b YAGAACAGGCTCCTCTAG Length of primer: 18


#The forward barcode and primer need to be combined to determine how much to cut off the ends of the sequence.
#Because barcodes and primers are different lengths, this has to be calculated for every set of barcodes/primers.

####IF the forward and reverse barcodes are not variable in length, this variable calculation can be replaced with the actual length of spacer + barcode + primer


###############################################
##2016 VERSION OF POSITIONAL CALCULATION 
###############################################

#       forward_sub=$(($length_fb + $length_fp + 1))
#       reverse_sub=$(($length_rb + $length_rp + 1))
#       removed_length=$(($forward_sub + $reverse_sub - 2))


#		export forward_sub
#		export reverse_sub
#		export removed_length


###############################################
##2017 VERSION OF POSITIONAL CALCULATION 
###############################################


        forward_sub=$(($length_fb + $length_fp))
        reverse_sub=$(($length_rb + $length_rp))

		export forward_sub
		export reverse_sub

#echo "Forward Barcode: " $sample $f_barcode "Length of barcode: "$length_fb
#echo "Forward Primer: " $sample $f_primer "Length of primer: "$length_fp
#echo "Forward Barcode and Primer Combined Length: " $sample $forward_sub
#echo "Reverse Barcode: " $sample $r_barcode "Length of barcode: "$length_rb
#echo "Reverse Primer: " $sample $r_primer "Length of primer: "$length_rp
#echo "Reverse Barcode and Primer Combined Length: " $sample $reverse_sub
#echo "Total Removed length: " $sample $removed_length


#This is the version of the code without parallel.

#for k in ./$temp/*.temp.split.*
#	do
#	filename=$k
#	export filename

	#demforward $filename >> "LIB_"$lib/$locus/$norep/$SampleID.fastq

#done



#########THIS VERSION CAN WORK INTERACTIVELY, BUT HAS PROBLEMS WHEN SENT TO BATCH WITH  A LARGE FASTA FILE TO DISSECT.  TURN OFF THIS BLOCK AND TURN ON THE SEMAPHORE VERSION BELOW WHEN SENDING TO SGE OR QSUB. NEVERMIND, just set -j1 to keep one job at a time running. Seems to be more effective than semaphore.

#for i in $temp/*fastq.split.* 
#do 
##parallel --willcite --blocksize $percpu --compress -m -j1 
#demforward $i >> "LIB_"$libfn/$locusfn/$norep/$SampleID.fastq
##parallel --willcite --blocksize $percpu --compress -m -j1 
#demreverse $i >> "LIB_"$libfn/$locusfn/$norep/$SampleID.fastq
#done



echo "LIB_"$libfn/$locusfn/$norep/$SampleID.fastq

##############################################################################
##THIS COMMAND STARTS THE LOOP WITHIN THE LOOP - EACH FASTQ IS SEARCHED BY BARCODE DEFINITIONS THAT ARE LOADED INTO THE SETVALUES FUNCTION.  THIS IS WRITTEN AS PARALLEL, BUT NOT ADVISED, SINCE TOTAL PROCESSORS = OUTER LOOP X 3 X INNER LOOP (10X3X1 =30 currently)
##############################################################################
#This is the version of the code with parallel.

free=$(awk '/^((Swap)?Cached|MemFree|Buffers):/ { sum += $2 }END{ print sum }' /proc/meminfo)
percpu=$((free / 200 / $(parallel --number-of-cores)))k


#The other parallel command is within the setvalues function and uses the values set in this iteration to read through the fastq file and assign it to the correct file

/local/cluster/bin/parallel --will-cite --blocksize $percpu -j1 demforward ::: temp*/*fastq*.rev  >> "LIB_"$libfn/$locusfn/$norep/$SampleID.fastq

#The original parallel code, for information. The above version worked better.
#find . -name *fastq.split.* | parallel --willcite --block $percpu -j1 -X demforward {} >> "LIB_"$libfn/$locusfn/$norep/$SampleID.fastq


##############################################################################

done
}

export -f setvalues


#The while loop has been replaced with a parallel command


#This parallel step loads the barcodes into the overarching loop.  That way, the number of jobs that are waiting can be fed the barcode parameters so they can start as soon as a slot opens up.

#Newer version of parallel commands.  The j value set here should be 1/3 of available cores, because the setvalues function uses three processors for each job.

##############################################################################
##THIS COMMAND STARTS THE LOOP THROUGH THE BARCODES - EACH JOB = ONE BARCODE
##EACH JOB NEEDS THREE PROCESSORS (SO RESERVE 3X THE VALUE FOR J BELOW
##############################################################################
/local/cluster/bin/parallel --will-cite -k -L1 -j10 setvalues :::: $barcodefile
##############################################################################





#This is to clean up empty and temporary files.
find ./LIB_*/*/*/* -type f -empty -delete 2>/dev/null
find ./LIB_*/*/* -type d -empty -delete 2>/dev/null
find ./LIB_*/* -type d -empty -delete 2>/dev/null
find ./LIB_* -type d -empty -delete 2>/dev/null
#find ./unassigned.fastq -type f -delete 2>/dev/null
#cat ./$temp/*temp.split* > /unassigned.fastq
#rm -rf ./temp$
